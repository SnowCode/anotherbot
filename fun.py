from helpers import *

import asyncpraw, random
r = asyncpraw.Reddit(client_id=praw_client, client_secret=praw_secret, user_agent="r/aspiememes crawler for Discord")

@bot.command()
async def meme(ctx):
    """Generates a random meme from r/aspiememes."""
    posts = []
    async with ctx.typing():
        subreddits = [ await r.subreddit("aspiememes") ]
        for subreddit in subreddits:
            async for post in subreddit.hot(limit=50):
                posts.append({"title": post.title, "url": post.url})
    post = random.choice(posts)
    await ctx.send(post["title"])
    await ctx.send(post["url"])
