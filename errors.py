from helpers import *

@bot.event
async def on_command_error(ctx, error):
    if isinstance(error, commands.MissingRequiredArgument):
        arg = error.param.name
        await logsend(ctx.message, f"This command requires an argument: {arg}.", discord.Colour.red())

    elif isinstance(error, commands.MissingPermissions):
        await logsend(ctx.message, f"You don't have permissions to run this command.", discord.Colour.red())

    elif isinstance(error, commands.CommandNotFound):
        await sleep(5)
        await logsend(ctx.message, f"Command not found. Try **!help**.", discord.Colour.red())

    elif isinstance(error, commands.BadArgument):
        print(error)
        await logsend(ctx.message, f"Invalid argument.", discord.Colour.red())

    else:
        print(error)
        await logsend(ctx.message, f"Unknown error.", discord.Colour.red())
