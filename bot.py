from helpers import *
from utility import *
from moderation import *
from fun import *
from reactions import *
from errors import *
from music import *

@bot.event
async def on_ready():
    print("Bot logged in.")

    await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.listening, name="you."))
    print("Status set.")

    print("Ready!")

bot.run(token)
