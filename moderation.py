from helpers import *

@bot.command()
@commands.has_permissions(administrator=True)
async def clear(ctx, num: int):
    """Clears a given number of messages in the current channel."""
    log_channel = bot.get_channel(log_channel_id)

    msg = []
    async for x in ctx.channel.history(limit=int(num)):
        msg.append(x)
    await ctx.channel.delete_messages(msg)
    await logsend(ctx.message, f"{num} messages removed from the channel.", discord.Colour.green())
    await log_channel.send(f"{ctx.message.author} cleared {num} messages in {ctx.channel}.")

@bot.command()
@commands.has_permissions(ban_members=True)
async def ban(ctx, user: discord.Member, reason="None"):
    """Bans a user."""
    embed = discord.Embed(title = "You got banned!", color = discord.Colour.orange())
    embed.add_field(name = "Reason:", value = str(reason), inline = True)
    await user.send(embed = embed)

    try:
        await user.ban()
        await logsend(ctx.message, "The user is banned.", discord.Colour.green())
    except NotFound:
        await logsend(ctx.message, "The user couldn't be found.", discord.Colour.red())

@bot.command()
@commands.has_permissions(kick_members=True)
async def kick(ctx, user: discord.Member, reason="None"):
    """Kicks a user."""
    embed = discord.Embed(title = "You got kicked!", color = discord.Colour.orange())
    embed.add_field(name = "Reason:", value = str(reason), inline = True)
    await user.send(embed = embed)

    try:
        await user.kick()
        await logsend(ctx.message, "The user is kicked.", discord.Colour.green())
    except NotFound:
        await logsend(ctx.message, "The user couldn't be found.", discord.Colour.red())

@bot.command()
@commands.has_permissions(ban_members=True)
async def mute(ctx, user: discord.Member, time: int, reason="None"):
    """Mutes a user for a given number of seconds."""
    embed = discord.Embed(title = f"You got muted for {time} seconds!", color = discord.Colour.orange())
    embed.add_field(name = "Reason:", value = str(reason))
    await user.send(embed = embed)

    role = discord.utils.get(ctx.guild.roles, name="Muted")
    await user.add_roles(role)
    await logsend(ctx.message, f"{user} is muted.", discord.Colour.green())

    await sleep(int(time))
    await user.remove_roles(role)
    await logsend(ctx.message, f"{user} is unmuted.", discord.Colour.green())

@bot.command()
@commands.has_permissions(administrator=True)
async def lock(ctx):
    """Prevents people from writing or adding reactions in a channel."""
    perms = ctx.channel.overwrites_for(ctx.guild.default_role)
    perms.send_messages = False
    perms.add_reactions = False
    await ctx.channel.set_permissions(ctx.guild.default_role, overwrite=perms)
    await logsend(ctx.message, "The channel is now locked :lock:.", discord.Colour.green())

@bot.command()
@commands.has_permissions(administrator=True)
async def unlock(ctx):
    """Makes people able to write and add reactions in the locked channel."""
    perms = ctx.channel.overwrites_for(ctx.guild.default_role)
    perms.send_messages = True
    perms.add_reactions = True
    await ctx.channel.set_permissions(ctx.guild.default_role, overwrite=perms)
    await logsend(ctx.message, "The channel is now unlocked.", discord.Colour.green())

@bot.command()
@commands.has_permissions(administrator=True)
async def lockdown(ctx, status):
    """When set to 'on', prevent all the non-administrators from writing or adding reactions on the server. Warning, it overwrites your current settings, only use in case of an emergency."""
    if status == "on":
        status = False
    else:
        status = True

    for channel in ctx.guild.text_channels:
        perms = channel.overwrites_for(ctx.guild.default_role)
        perms.send_messages = status
        perms.add_reactions = status

        await channel.set_permissions(ctx.guild.default_role, overwrite=perms)

    if status == False:
        await logsend(ctx.message, "The server is now locked :rotating_light:.", discord.Colour.orange())
    else:
        await logsend(ctx.message, "The server is now unlocked :unlock:.", discord.Colour.green())

@bot.event
async def on_message_delete(message):
    log_channel = bot.get_channel(log_channel_id)
    await log_channel.send(f"{message.author} deleted a message in {message.channel}.")
