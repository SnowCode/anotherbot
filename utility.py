from config import *
from helpers import *

@bot.command()
async def uptime(ctx):
    """Gives you the current uptime of the bot."""
    uptime = datetime.datetime.utcnow() - bot.launch_time
    await logsend(ctx.message, f"{str(uptime)}", discord.Colour.green())

@bot.command()
async def ping(ctx):
    """Ping pong."""
    await ctx.send(f"Pong! {round(bot.latency * 1000)}ms.")

@bot.command()
async def avatar(ctx, *,  avamember : discord.Member=None):
    """Gets the avatar of any user on the server."""
    if avamember == None:
        avamember = ctx.message.author

    userAvatarUrl = avamember.avatar_url
    await ctx.send(userAvatarUrl)

@bot.command()
@commands.has_permissions(administrator=True)
async def sync(ctx):
    """Syncs all the channels in the category where you type this command."""
    category = ctx.channel.category.text_channels
    for channel in category:
        await channel.edit(sync_permissions=True)
    await ctx.send("All the channels in this category are now synced.")
