# AnotherBot
`AnotherBot` is a custom bot for [this](https://discord.gg/u3ukY7wceG) awesome Discord server, written in Python (version 3)

## Features
* Get avatar of anybody on the server
* Moderation tools (like clear, mute, kick, ban, lock, unlock or lockdown)
* System tools (like uptime or ping) 
* Self-assignable roles (also called "reaction bot")
* Random memes from `r/aspiememes`
* Entierly free software (except it's on Discord, unfortunately)
* A very very basic music bot (only play and stop)

## Setting up
> You are free to make your own bot by using our code. Just make sure to respect our [license](./LICENSE).

Also, this bot is connected to several APIs (Reddit and Discord at the moment).

### 1. Discord Server
Of course you're going to need a Discord server, I'm not going to explain how to create one here. You will just need to create a few special roles and channels to use the bot at its full power.

First create a role called "Muted" with the necessary permissions. Then create the following channels as you wish

* `#roles` to make members able to self-assign roles
* `#log` that will track every time when somebody remove a message (that makes you able to spot ghost pingers very quickly)
* `#submissions` is a channel where members can submit free software and vote for them
* `#fsotw` is a channel where the top 3 best free software of the week will be displayed (that have previously been submitted by members in `#submissions`)

**Make sure** to right click on those channels and copy their IDs for later.

Once you done all of this, your server is ready to get the bot!

### 2. Discord Bot
The first thing is to create a Discord bot. So let's go on [Discord Dev](https://discord.com/developers/).

From that screen just click on `New Application`, then click on `Bot` (in the sidebar) and create one. Then enable all the `Presence Intents`, it's very important.

Then, copy the `token` of your bot and store it somewhere (you will need it later).

Finally, click on `OAuth2` then select `bot` as "scope" and `Administrator` as permission. Once you done that, copy the provided link and go on it.

On that place you can select your newly created Discord server and allow the bot to join it.

### 3. Reddit application
The server has an option to display random memes from some subreddits. We chose to use `asyncpraw` for this, but this Python module requires a valid Reddit application ID and token...

First, go on [this page](https://www.reddit.com/prefs/apps) to create a new app (select "script" as its scope) and put what ever URL in the "url" section.

Once you created it, save the "secret" (a 30 characters string) and the "id" (a 14 characters string) somewhere on your computer (same as for the Discord token).

### 4. Dependencies
You now need to install the app's dependencies. For this guide you will need to have `ffmpeg` and `python3` installed.

```bash
sudo apt install python3 python3-pip ffmpeg
```

Then, simply run the following command to install everything:

```bash
pip3 install discord emoji asyncpraw youtube_dl PyNaCl
```

### 5. Run it
You now need to download the source code

```bash
git clone https://codeberg.org/SnowCode/anotherbot
cd anotherbot/
```

Now you need to create a `config.py` file with the following template content (that's where you will place all your tokens and ids)

```python
token = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" # Your Discord token
bot_channel_id = 834815809678737468 # Your #freesoftware-otw channel ID
submit_channel_id = 834815917006389278 # Your #submissions channel ID
log_channel_id = 839212345158008832 # Your #log channel ID

praw_client = "xxxxxxxxxxxxxx" # Your Reddit application ID here
praw_secret = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" # Your Reddit application secret here
```

Once you done all of this, you can run the following command to execute the script (it shouldn't get into any errors if everything have been done properly, if it does please report it [here](codeberg.org/SnowCode/anotherbot/issues/).

```bash
python3 bot.py
```

If it's says "ready" then it's up and running!

### 6. Systemd file (optional)
If you want to use it for production you may want to use systemd with it.

First, create a new user on your server.

```bash
sudo su
useradd -m discord
passwd discord
su - discord
```

Then inside that home directory, repeat the step 4 and 5.

Then, copy the systemd file to the right location by running the following command:

```bash
exit # Get out of the discord user
cp /home/discord/anotherbot/anotherbot.service /etc/systemd/system/
chmod +x /home/discord/anotherbot/start.sh # This is the start script that is used by the service file
chmod +x /home/discord/anotherbot/reload.sh # This is the script that will be used to update and restart the bot
systemctl restart anotherbot
```

You can finally create an alias to quickly reload your server when changes are made on the upstream repository:

```bash
echo 'alias reload="ssh discord@<your server ip> -t ./anotherbot/reload.sh"' >> ~/.bashrc
```

Now you will simply have to run `reload` to apply all the changes on your server.
